#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>

int main()
{
	pid_t child_pid;
    
    child_pid = fork();

    if (child_pid == 0)
    {
    	while (1)
    	{
    		printf("I'm still alive! \n");
    	}
    }
    Sleep(1);
    printf("Dispatching...\n");
    kill(child_pid, SIGCHLD);
    printf("Dispatched...\n");
    printf("Disappear...\n");
    printf("Dysfunction...\n");
    printf("Destruction...\n");

	return 0;
}
