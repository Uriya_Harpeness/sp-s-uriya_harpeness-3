#include <unistd.h>
#include <sys/sysinfo.h>
#include <sys/syscall.h>

void exit()
{
	char menu[100];

	strcpy(menu, "exit()");

	syscall(SYS_write, 0, menu, slen(menu));
}

void id()
{
	char menu[100];

	syscall(SYS_gettid);
    syscall(SYS_tgkill, getpid(), menu, slen(menu));

    syscall(SYS_write, 0, menu, slen(menu));
}

int main()
{
	char menu[100];
	int option;
	void (*options[3])();
	options[0] = id;
	options[1] = exit;
	
	strcpy(menu, "Alloha! \n");
	
    syscall(SYS_write, 0, menu, slen(menu));

    do
    {
    	scanf("%d", &option);
    	options[option]();
    }
    while (1);

    id();
}