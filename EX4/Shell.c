#include <stdio.h>
#include <stdlib.h>
#include "LineParser.h"

void execute(cmdLine *pCmdLine)
{
	char str[2048];
	printf("entered: %s\n", pCmdLine->arguments[0]);

	pid_t child_pid;
    
    child_pid = fork();

    if (child_pid == 0)
    {
    	execvp(pCmdLine->arguments[0], pCmdLine->arguments);
	    perror("ERROR! ");
	    exit(0);
    }
    wait(child_pid);
}

int main()
{
	char str[2048];
	struct cmdLine* cmd;

	strcpy(str, get_current_dir_name());
	printf("path: %s\n", str);	

	do
	{
		gets(str);
		if (strcmp(str, "please stop") && strcmp(str, "quit") && strcmp(str, ""))
		{
			cmd = parseCmdLines(str);
			execute(cmd);
		}
	}
	while (strcmp(str, "please stop") && strcmp(str, "quit"));
	freeCmdLines(cmd);

	return 0;
}
